#!/usr/bin/env python
 
# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()
 
# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()
 
# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )
inputFilePath = '/eos/user/y/yonoda/mc21_13p6TeV.601481.PhPy8EG_PDF4LHC21_ttH125_tincl_gammagamma.deriv.DAOD_PHYS.e8472_s3873_s3874_r13829_r13831_p5631'
ROOT.SH.ScanDir().filePattern( 'DAOD*' ).scan( sh, inputFilePath )
sh.printContent()
 
# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 ) # maximum number of events to be analyzed. If you want to see all events, please set it to -1
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# create public tools
from AnaAlgorithm.DualUseConfig import createPublicTool

# trigger configuration tool
xAODConfigTool = createPublicTool( 'TrigConf::xAODConfigTool', 'xAODConfigTool' )

# trigger decision tool
TrigDecisionTool = createPublicTool( 'Trig::TrigDecisionTool', 'TrigDecisionTool' )
TrigDecisionTool.TrigDecisionKey = 'xTrigDecision'
TrigDecisionTool.NavigationFormat = 'TrigComposite'
TrigDecisionTool.HLTSummary = 'HLTNav_Summary_AODSlimmed'
 
# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'Hgam2xAODAnalysis', 'AnalysisAlg' )
alg.OutputLevel = ROOT.MSG.WARNING
 
# later on we'll add some configuration options for our algorithm that go here
from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
seq = AnaAlgSequence( "Hgam2xAODAnalysisSequence" )
seq.addPublicTool(xAODConfigTool)
seq.addPublicTool(TrigDecisionTool)
 
# Add our algorithm to the job
seq.append(alg, inputPropName = None )
for alg in seq:
	job.algsAdd( alg )
	pass
job.outputAdd(ROOT.EL.OutputStream('ANALYSIS'))
 
# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
