#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
//#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
//#include "ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "AsgTools/AnaToolHandle.h"

class IElectronPhotonShowerShapeFudgeTool;

namespace CP {
	class IEgammaCalibrationAndSmearingTool;
	class IPileupReweightingTool;
	class IIsolationCorrectionTool;
	class IIsolationSelectionTool;
}

class Hgam2xAODAnalysis : public EL::AnaAlgorithm
{
	public:
		// this is a standard algorithm constructor
		Hgam2xAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

		// these are the functions inherited from Algorithm
		virtual StatusCode initialize () override;
		virtual StatusCode execute () override;
		StatusCode DumpProdChain(const xAOD::TruthParticle* child, bool &isPhotonFromHiggs);
		virtual StatusCode finalize () override;

	private:
		unsigned int m_runNumber = 0;
		unsigned int m_randomRunNumber = 0;
		unsigned int m_lumiBlock = 0;
		unsigned long long m_eventNumber = 0;
		std::unique_ptr<std::vector<float>> m_photonE;
		std::unique_ptr<std::vector<float>> m_photonPt;
		std::unique_ptr<std::vector<float>> m_photonEta;
		std::unique_ptr<std::vector<float>> m_photonPhi;

		std::unique_ptr<std::vector<int>> m_photonPdgId;
		std::unique_ptr<std::vector<int>> m_parentPdgId;
		//int m_Nmctruthphoton;
		//std::unique_ptr<std::vector<bool>> m_is_photon_from_Higgs;

		bool m_L12eEM24L;
		bool m_L12eEM18M;
		bool m_L1eEM26M;
		bool m_HLTg35g25M_L124L;
		bool m_HLT2g20T_L118M;
		bool m_HLT2g22T_L118M;

		std::unique_ptr<std::vector<bool>> m_is_tight;
		std::unique_ptr<std::vector<bool>> m_is_loose;
		std::unique_ptr<std::vector<bool>> m_isoselection;

		std::unique_ptr<std::vector<bool>> m_etacut;

		ToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool;
		ToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool;
		asg::AnaToolHandle<CP::IPileupReweightingTool> m_prwTool;
		asg::AnaToolHandle<IElectronPhotonShowerShapeFudgeTool> m_electronPhotonShowerShapeFudgeTool;
		asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_egammaCalibTool;
		asg::AnaToolHandle<CP::IIsolationCorrectionTool> m_isoCorrTool;
		asg::AnaToolHandle<CP::IIsolationSelectionTool> m_isoTool;
};

#endif
