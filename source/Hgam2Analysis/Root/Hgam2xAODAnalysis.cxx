#include <AsgMessaging/MessageCheck.h>
#include <Hgam2Analysis/Hgam2xAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/EgammaTruthxAODHelpers.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/xAODTruthHelpers.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthVertexContainer.h>
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"
#include "EgammaAnalysisInterfaces/IElectronPhotonShowerShapeFudgeTool.h"
#include "EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h"
#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "IsolationSelection/IIsolationSelectionTool.h"
#include "xAODCore/ShallowCopy.h"
#include <TH1.h> 
#include <TTree.h> 

Hgam2xAODAnalysis :: Hgam2xAODAnalysis (const std::string& name,
		ISvcLocator *pSvcLocator)
	: EL::AnaAlgorithm (name, pSvcLocator),
	m_trigConfigTool ("TrigConf::xAODConfigTool/xAODConfigTool",this),
	m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool",this),
	m_prwTool(""),
	m_electronPhotonShowerShapeFudgeTool(""),
	m_egammaCalibTool(""),
	m_isoCorrTool(""),
	m_isoTool("")
{
	// Here you put any code for the base initialization of variables,
	// e.g. initialize all pointers to 0.  This is also where you
	// declare all properties for your algorithm.  Note that things like
	// resetting statistics variables or booking histograms should
	// rather go into the initialize() function.
	declareProperty("xAODConfigTool", m_trigConfigTool, "the TrigConf::xAODConfig tool");
	declareProperty("TrigDecisionTool", m_trigDecisionTool, "the TrigDecision tool");
	m_prwTool.declarePropertyFor( this, "PileupReweightingTool", "The PRW tool" );
	m_electronPhotonShowerShapeFudgeTool.declarePropertyFor( this, "PhotonShowerShapeFudgeTool", "The ElectronPhotonShowerShapeFudgeTool" );
	m_egammaCalibTool.declarePropertyFor( this, "EgammaCalibrationAndSmearingTool", "The EgammaCalibrationAndSmearingTool");
	m_isoCorrTool.declarePropertyFor( this, "IsolationCorrectionTool", "The IsolationCorrectionTool" );
	m_isoTool.declarePropertyFor( this, "IsolationSelectionTool", "The IsolationSelectionTool");
}



StatusCode Hgam2xAODAnalysis :: initialize ()
{
	// Here you do everything that needs to be done at the very
	// beginning on each worker node, e.g. create histograms and output
	// trees.  This method gets called before any input files are
	// connected

	ANA_CHECK (book (TH1F ("h_nPhotons", "Number of photons", 50, 0., 50.)));
	ANA_CHECK (book (TH1F ("h_photon_e", "Photon energy [GeV]", 100, 0., 250.)));
	ANA_CHECK (book (TH1F ("h_photon_pt", "Photon p_{T} [GeV]", 100, 0., 250.)));
	ANA_CHECK (book (TH1F ("h_photon_eta", "Photon #eta", 300, -3.0, 3.0)));
	ANA_CHECK (book (TH1F ("h_photon_phi", "Photon #phi [radian]", 100, -M_PI, M_PI)));

	ANA_CHECK (book (TH1F ("h_photonPdgId", "PhotonPdgId", 350, 0., 350.)));
	ANA_CHECK (book (TH1F ("h_parentPdgId", "parentPdgId", 350, 0., 350.)));
	//ANA_CHECK (book (TH1F ("h_Nmctruthphoton", "Nmctruthphoton", 7, -1., 6.)));

	ANA_CHECK (book (TH1F ("h_truthphoton_e", "truth Photon energy [GeV]", 100, 0., 250.)));
	ANA_CHECK (book (TH1F ("h_truthphoton_pt", "truth Photon p_{T} [GeV]", 100, 0., 250.)));
	ANA_CHECK (book (TH1F ("h_truthphoton_eta", "truth Photon #eta", 300, -3.0, 3.0)));
	ANA_CHECK (book (TH1F ("h_truthphoton_phi", "truth Photon #phi [radian]", 100, -M_PI, M_PI)));

	ANA_CHECK( m_trigConfigTool.retrieve() );
	ANA_CHECK( m_trigDecisionTool.retrieve() );

	ANA_CHECK(book(TH1F("h_HLTg35medium","g35 medium",4,-1.,3.)));
	ANA_CHECK(book(TH1F("h_HLTg20tight","g20 tight",4,-1.,3.)));
	ANA_CHECK(book(TH1F("h_HLTg22tight","g22 tight",4,-1.,3.)));

	ANA_CHECK(book(TH1F("h_L12eEM24L","L1 24L",4,-1.,3.)));
	ANA_CHECK(book(TH1F("h_L12eEM18M","L1 18M",4,-1.,3.)));

	ANA_CHECK (book (TTree ("analysis", "Htobbgg analysis tree")));

	TTree* mytree = tree ("analysis");

	mytree->Branch ("RunNumber", &m_runNumber);
	mytree->Branch ("RandomRunNumber", &m_randomRunNumber);
	mytree->Branch ("LumiBlock", &m_lumiBlock);
	mytree->Branch ("EventNumber", &m_eventNumber);
	m_photonE = std::make_unique<std::vector<float>>();
	mytree->Branch ("photonE", &*m_photonE);
	m_photonPt = std::make_unique<std::vector<float>>();
	mytree->Branch ("photonPt", &*m_photonPt);
	m_photonEta = std::make_unique<std::vector<float>>();
	mytree->Branch ("photonEta", &*m_photonEta);
	m_photonPhi = std::make_unique<std::vector<float>>();
	mytree->Branch ("photonPhi", &*m_photonPhi);

	m_photonPdgId = std::make_unique<std::vector<int>>();
	mytree->Branch ("photonPdgId", &*m_photonPdgId);
	m_parentPdgId = std::make_unique<std::vector<int>>();
	mytree->Branch ("parentPdgId", &*m_parentPdgId);
	//mytree->Branch ("Nmctruthphoton", &m_Nmctruthphoton);
	//m_is_photon_from_Higgs = std::make_unique<std::vector<bool>>();
	//mytree->Branch ("is_photon_from_Higgs", &*m_is_photon_from_Higgs);

	mytree->Branch("L12eEM24L", &m_L12eEM24L);
	mytree->Branch("L12eEM18M", &m_L12eEM18M);
	mytree->Branch("L1eEM26M", &m_L1eEM26M);
	mytree->Branch("HLTg35g25M_L124L", &m_HLTg35g25M_L124L);
	mytree->Branch("HLT2g20T_L118M", &m_HLT2g20T_L118M);
	mytree->Branch("HLT2g22T_L118M", &m_HLT2g22T_L118M);

	m_is_tight = std::make_unique<std::vector<bool>>();
	mytree->Branch ("isTight", &*m_is_tight);
	m_is_loose = std::make_unique<std::vector<bool>>();
	mytree->Branch ("isLoose", &*m_is_loose);
	m_isoselection = std::make_unique<std::vector<bool>>();
	mytree->Branch ("isoSelection", &*m_isoselection);

	m_etacut = std::make_unique<std::vector<bool>>();
	mytree->Branch ("etacut", &*m_etacut);

	if (!m_prwTool.isUserConfigured()) {
		//for mc16
		//std::vector<std::string> vLumiCalcFiles = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20190708/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-010.root"};
		//std::vector<std::string> vConfigFiles = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/share/DSID600xxx/pileup_mc16a_dsid600051_FS.root"};
		
		//for mc21
		std::vector<std::string> vLumiCalcFiles = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data22_13p6TeV/20230207/ilumicalc_histograms_None_428648-440613_OflLumi-Run3-003.root"};
		std::vector<std::string> vConfigFiles = {"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data22_13p6TeV/20230207/purw.actualMu.2022.ignore_TRIGMUO_TRIGLAR.root"};
		m_prwTool.setTypeAndName("CP::PileupReweightingTool/PrwTool");
		ATH_CHECK( m_prwTool.setProperty("ConfigFiles",vConfigFiles ) );
		ATH_CHECK( m_prwTool.setProperty("LumiCalcFiles", vLumiCalcFiles) );
		//ATH_CHECK( m_prwTool.setProperty("DataScaleFactor",     m_prwDataSF) ); // 1./1.03 -> default for mc16, see: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ExtendedPileupReweighting#Tool_Properties
		// ATH_CHECK( m_prwTool.setProperty("DataScaleFactorUP",   m_prwDataSF_UP) ); // 1. -> old value (mc15), as the one for mc16 is still missing
		//ATH_CHECK( m_prwTool.setProperty("DataScaleFactorDOWN", m_prwDataSF_DW) ); // 1./1.18 -> old value (mc15), as the one for mc16 is still missing
		//ATH_CHECK( m_prwTool.setProperty("TrigDecisionTool", m_trigDecisionTool.getHandle()) );
		//ATH_CHECK( m_prwTool.setProperty("UseRunDependentPrescaleWeight", m_runDepPrescaleWeightPRW) );
		ATH_CHECK( m_prwTool.setProperty("OutputLevel", MSG::WARNING) );
		ATH_CHECK( m_prwTool.retrieve() );
	} else {
		ATH_MSG_INFO("Using user-configured PRW tool");
		ATH_CHECK( m_prwTool.retrieve() );
	}

	if (!m_electronPhotonShowerShapeFudgeTool.isUserConfigured()) {
		m_electronPhotonShowerShapeFudgeTool.setTypeAndName("ElectronPhotonShowerShapeFudgeTool/ElectronPhotonShowerShapeFudgeTool");

		int FFset = 22;
		ATH_CHECK( m_electronPhotonShowerShapeFudgeTool.setProperty("Preselection", FFset) );
		ATH_CHECK( m_electronPhotonShowerShapeFudgeTool.setProperty("OutputLevel", this->msg().level()) );
		ATH_CHECK( m_electronPhotonShowerShapeFudgeTool.retrieve() );
	} else ATH_CHECK( m_electronPhotonShowerShapeFudgeTool.retrieve() );

	if (!m_egammaCalibTool.isUserConfigured()) {
		m_egammaCalibTool.setTypeAndName("CP::EgammaCalibrationAndSmearingTool/EgammaCalibrationAndSmearingTool");
		ATH_MSG_DEBUG( "Initialising EgcalibTool " );
		ATH_CHECK( m_egammaCalibTool.setProperty("ESModel", "es2022_R22_PRE") ); //used for analysis using data processed with 21.0
		ATH_CHECK( m_egammaCalibTool.setProperty("decorrelationModel", "1NP_v1") );
		ATH_CHECK( m_egammaCalibTool.setProperty("useAFII", 0) );
		ATH_CHECK( m_egammaCalibTool.setProperty("OutputLevel", this->msg().level()) );
		ATH_CHECK( m_egammaCalibTool.retrieve() );
	} else ATH_CHECK( m_egammaCalibTool.retrieve() );

	if ( !m_isoCorrTool.isUserConfigured() ) {
		m_isoCorrTool.setTypeAndName("CP::IsolationCorrectionTool/IsoCorrTool");
		ATH_CHECK( m_isoCorrTool.setProperty( "ToolVer", "REL22") );
		ATH_CHECK( m_isoCorrTool.setProperty( "IsMC", true ) );
		ATH_CHECK( m_isoCorrTool.setProperty( "AFII_corr", false) );
		//ATH_CHECK( m_isoCorrTool.setProperty( "Apply_SC_leakcorr", false) );
		ATH_CHECK( m_isoCorrTool.setProperty( "CorrFile", "IsolationCorrections/v6/isolation_ptcorrections_rel22_mc20.root") );
		ATH_CHECK( m_isoCorrTool.setProperty( "OutputLevel", this->msg().level()) );
		ATH_CHECK( m_isoCorrTool.retrieve() );
	} else  ATH_CHECK( m_isoCorrTool.retrieve() );

	if (!m_isoTool.isUserConfigured()) {
		m_isoTool.setTypeAndName("CP::IsolationSelectionTool/IsoTool");
		//ATH_CHECK( m_isoTool.setProperty("ElectronWP", m_eleIso_WP.empty()    ? "Loose_VarRad" : m_eleIso_WP) );
		//ATH_CHECK( m_isoTool.setProperty("MuonWP",     m_muIso_WP.empty()     ? "Loose_VarRad" : m_muIso_WP) );
		ATH_CHECK( m_isoTool.setProperty("PhotonWP", "FixedCutLoose" ) );
		ATH_CHECK( m_isoTool.setProperty("OutputLevel", this->msg().level()) );
		ATH_CHECK( m_isoTool.retrieve() );
	} else  ATH_CHECK( m_isoTool.retrieve() );

	return StatusCode::SUCCESS;
}


StatusCode Hgam2xAODAnalysis :: execute ()
{
	// Here you do everything that needs to be done on every single
	// events, e.g. read input variables, apply cuts, and fill
	// histograms and trees.  This is where most of your actual analysis
	// code will go.

	ANA_MSG_INFO("in execute");

	// retrieve the eventInfo object from the event store
	const xAOD::EventInfo *eventInfo = nullptr;
	ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

	//	if (m_prwTool->apply(*eventInfo).isFailure()) {
	//		ATH_MSG_ERROR("Cannot execute apply of pileupRW");
	//	}

	m_runNumber = eventInfo->runNumber();
	m_randomRunNumber = m_prwTool->getRandomRunNumber(*eventInfo);
	eventInfo->auxdecor<unsigned int>("RandomRunNumber") = m_randomRunNumber;
	m_lumiBlock = eventInfo->lumiBlock();
	m_eventNumber = eventInfo->eventNumber();

	// print out run and event number from retrieved object
	ANA_MSG_INFO ("in execute, runNumber = " << m_runNumber <<
			", randomRunNumber = " << m_randomRunNumber << 
			", lumiBlock = " << m_lumiBlock <<
			", eventNumber = " << m_eventNumber);

	// Photons, load container from evtStore
	const xAOD::PhotonContainer *photons = nullptr;
	ANA_CHECK(evtStore()->retrieve(photons, "Photons"));

	std::pair<xAOD::PhotonContainer*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*photons);
	std::unique_ptr<xAOD::PhotonContainer> shallowphotons (shallowcopy.first);

	//bool setLinks = xAOD::setOriginalObjectLink(*photons, *copy);	
	// loop on the photon container

	static const SG::AuxElement::Accessor<char>  DFCommonPhotonsIsEMTight ("DFCommonPhotonsIsEMTight");
	static const SG::AuxElement::Accessor<char>  DFCommonPhotonsIsEMLoose ("DFCommonPhotonsIsEMLoose");

	int nPhotons = 0;

	m_photonE->clear();
	m_photonPt->clear();
	m_photonEta->clear();
	m_photonPhi->clear();

	m_photonPdgId->clear();
	m_parentPdgId->clear();

	m_is_tight->clear();
	m_is_loose->clear();
	m_isoselection->clear();
	//m_is_photon_from_Higgs->clear();

	m_etacut->clear();

	m_L12eEM24L = 0;
	m_L12eEM18M = 0;
	m_L1eEM26M = 0;
	m_HLTg35g25M_L124L = 0;
	m_HLT2g20T_L118M = 0;
	m_HLT2g22T_L118M = 0;

	//int nTruthphoton = 0;

	for (xAOD::Photon *photon : *shallowphotons) {
		//ANA_MSG_INFO (" photon energy = " << photon->e()*0.001 << " GeV");
		//ANA_MSG_INFO (" photon pt = " << photon->pt()*0.001 << " GeV");
		//ANA_MSG_INFO (" photon eta = " << photon->eta());
		//ANA_MSG_INFO (" photon phi = " << photon->phi());

		//if ( m_electronPhotonShowerShapeFudgeTool->applyCorrection(*photon) != CP::CorrectionCode::Ok)
		//	ATH_MSG_ERROR("FillPhoton - fudge tool: applyCorrection failed");

		//ANA_MSG_INFO("before calib Energy = " << photon->e()*0.001 << " GeV");

		//if (fabs( photon->caloCluster()->etaBE(2) ) > 1.37 && fabs( photon->caloCluster()->etaBE(2) ) < 1.52) continue;

		if (m_egammaCalibTool->applyCorrection(*photon) != CP::CorrectionCode::Ok)
			ATH_MSG_ERROR("FillPhoton: EgammaCalibTool applyCorrection failed");

		//ANA_MSG_INFO("after calib Energy = " << photon->e()*0.001 << " GeV");

		float topoetcone20_value = -999;
		float ptcone20_value = -999;
		photon->isolationValue(topoetcone20_value, xAOD::Iso::topoetcone20);
		photon->isolationValue(ptcone20_value, xAOD::Iso::ptcone20);
		ANA_MSG_INFO("before TOPOisovalue = " << topoetcone20_value);
		ANA_MSG_INFO("before PTisovalue = " << ptcone20_value);
		if (m_isoCorrTool->applyCorrection(*photon)  != CP::CorrectionCode::Ok)
			ATH_MSG_ERROR("FillPhoton: IsolationCorrectionTool applyCorrection failed");

		photon->isolationValue(topoetcone20_value, xAOD::Iso::topoetcone20);
		photon->isolationValue(ptcone20_value, xAOD::Iso::ptcone20);
		ANA_MSG_INFO("after TOPOisovalue = " << topoetcone20_value);
		ANA_MSG_INFO("after PTisovalue = " << ptcone20_value);

		//bool isoselection = bool(m_isoTool->accept(*photon));
		//ANA_MSG_INFO("isoselection = " << isoselection);

		int photonId = -999;
		int parentId = -999;
		bool isPhotonFromHiggs = false;
		int numparents; 

		const xAOD::TruthParticle *truthphoton = xAOD::TruthHelpers::getTruthParticle(*photon);

		if(truthphoton){
			//ANA_MSG_INFO (" photon pdgId = " << truthphoton->pdgId());
			photonId = truthphoton->pdgId();
			numparents = truthphoton->nParents();	
			parentId = (truthphoton->nParents() > 0 && truthphoton->parent(0)) ? truthphoton->parent(0)->pdgId() : 0;
			if((photonId != 22)) continue;
			bool hastruthphotonVtx = truthphoton->hasProdVtx();
			if(!hastruthphotonVtx) continue;
			//		const xAOD::TruthVertex* vtx = truthphoton->prodVtx();
			//		for(unsigned int i=0; i<vtx->nIncomingParticles(); i++) {
			//			const xAOD::TruthParticle* parent = vtx->incomingParticle(i);
			//			ANA_MSG_INFO("parent pdgId " << parent->pdgId() << " parent status " << parent->status());

			//			const xAOD::TruthVertex* parentvtx = parent->prodVtx();
			//			for(unsigned int i=0; i<parentvtx->nIncomingParticles(); i++){
			//				const xAOD::TruthParticle* grandparent = parentvtx->incomingParticle(i);
			//				ANA_MSG_INFO("grandparent pdgId " << grandparent->pdgId() << " grandparent status " << grandparent->status());

			//				if(grandparent->pdgId() != 22) continue;
			//				const xAOD::TruthVertex* grandparentvtx = grandparent->prodVtx();
			//				for(unsigned int i=0; i<grandparentvtx->nIncomingParticles(); i++){
			//					const xAOD::TruthParticle* grandgrandparent = grandparentvtx->incomingParticle(i);
			//					ANA_MSG_INFO("grandgrandparent pdgId " << grandgrandparent->pdgId() << " grandgrandparent status " << grandgrandparent->status());
			//				}
			//			}
			//		}
			if(truthphoton->pt()<0.00001) continue;
			DumpProdChain(truthphoton,isPhotonFromHiggs);
		}

		if(photonId != 22) continue;
		if(!isPhotonFromHiggs) continue;

		ANA_MSG_INFO(" number of parents = " << numparents);	
		ANA_MSG_INFO(" photon Id = " << photonId);
		ANA_MSG_INFO(" parent Id = " << parentId);
		//if((photonId != 22) || (parentId != 25)) continue;
		//if((photonId != 22)) continue;

		ANA_MSG_INFO(" photon pt = " << photon->pt()*0.001 << " GeV");

		hist ("h_photon_e")->Fill(photon->e() * 0.001);
		hist ("h_photon_pt")->Fill(photon->pt() * 0.001);
		hist ("h_photon_eta")->Fill(photon->eta());
		hist ("h_photon_phi")->Fill(photon->phi());
		hist ("h_photonPdgId")->Fill(photonId);
		hist ("h_parentPdgId")->Fill(parentId);

		m_photonE->push_back(photon->e()*0.001);
		m_photonPt->push_back(photon->pt()*0.001);
		m_photonEta->push_back(photon->eta());
		m_photonPhi->push_back(photon->phi());
		m_photonPdgId->push_back(photonId);
		m_parentPdgId->push_back(parentId);
		//m_is_photon_from_Higgs->push_back(isPhotonFromHiggs);

		bool is_tight_photon = DFCommonPhotonsIsEMTight(*photon);
		m_is_tight->push_back(is_tight_photon);
		bool is_loose_photon = DFCommonPhotonsIsEMLoose(*photon);
		m_is_loose->push_back(is_loose_photon);
		bool isoselection = bool(m_isoTool->accept(*photon));
		m_isoselection->push_back(isoselection);
		//ANA_MSG_INFO("isoselection = " << isoselection);

		bool Etacut = 1;
		if (fabs( photon->caloCluster()->etaBE(2) ) > 1.37 && fabs( photon->caloCluster()->etaBE(2) ) < 1.52 ){
			Etacut = 0;
		}
		m_etacut->push_back(Etacut);

		nPhotons++;
	}

	hist ("h_nPhotons")->Fill(nPhotons);

	m_L12eEM24L = m_trigDecisionTool->isPassed("L1_2eEM24L");
	if(m_L12eEM24L == 1) ANA_MSG_INFO("bool " << m_L12eEM24L << " ,this event passed L1_2eEM24L");
	m_L12eEM18M = m_trigDecisionTool->isPassed("L1_2eEM18M");
	if(m_L12eEM18M == 1) ANA_MSG_INFO("bool " << m_L12eEM18M << " ,this event passed L1_2eEM18M");
	m_L1eEM26M = m_trigDecisionTool->isPassed("L1_eEM26M");
	if(m_L1eEM26M == 1) ANA_MSG_INFO("bool " << m_L1eEM26M << " ,this event passed L1_eEM26M");
	m_HLTg35g25M_L124L = m_trigDecisionTool->isPassed("HLT_g35_medium_g25_medium_L12eEM24L");
	if(m_HLTg35g25M_L124L == 1) ANA_MSG_INFO("bool " << m_HLTg35g25M_L124L << " ,this event passed HLT_g35_medium_g25_medium_L12eEM24L");
	m_HLT2g20T_L118M = m_trigDecisionTool->isPassed("HLT_2g20_tight_icaloloose_L12eEM18M");
	if(m_HLT2g20T_L118M == 1) ANA_MSG_INFO("bool " << m_HLT2g20T_L118M << " ,this event passed HLT_2g20_tight_icaloloose_L12eEM18M");
	m_HLT2g22T_L118M = m_trigDecisionTool->isPassed("HLT_2g22_tight_L12eEM18M");
	if(m_HLT2g22T_L118M == 1) ANA_MSG_INFO("bool " << m_HLT2g22T_L118M << " ,this event passed HLT_2g22_tight_L12eEM18M");

	//m_Nmctruthphoton = nTruthphoton;
	//hist("h_Nmctruthphoton")->Fill(nTruthphoton);
	tree("analysis")->Fill();	  

	//ANA_MSG_INFO ("Number of truth photon from Higgs is " << nTruthphoton);
	return StatusCode::SUCCESS;
}


StatusCode Hgam2xAODAnalysis :: DumpProdChain(const xAOD::TruthParticle* child, bool &isPhotonFromHiggs)
{
	bool hasProdVtx = child->hasProdVtx();
	if(!hasProdVtx) return StatusCode::SUCCESS;

	const xAOD::TruthVertex* vtx = child->prodVtx();
	for(unsigned int i=0; i<vtx->nIncomingParticles(); i++) {
		float child_pt = child->pt();
		if(child_pt<0.00001) continue;
		if(child->pdgId() != 22) continue;
		const xAOD::TruthParticle* parent = vtx->incomingParticle(i);
		if(parent->pdgId() != 22 && parent->pdgId() != 25) continue;
		ANA_MSG_INFO("parent pdgId " << parent->pdgId() << " parent status " << parent->status());
		if(parent->pdgId() == 25){ 
			isPhotonFromHiggs = true;
			return StatusCode::SUCCESS;
		}
		DumpProdChain(parent,isPhotonFromHiggs);
	}

	return StatusCode::SUCCESS;
}


StatusCode Hgam2xAODAnalysis :: finalize ()
{
	// This method is the mirror image of initialize(), meaning it gets
	// called after the last event has been processed on the worker node
	// and allows you to finish up any objects you created in
	// initialize() before they are written to disk.  This is actually
	// fairly rare, since this happens separately for each worker node.
	// Most of the time you want to do your post-processing on the
	// submission node after all your histogram outputs have been
	// merged.
	return StatusCode::SUCCESS;
}
