#define MyAna12_cxx
#include "MyAna12.h"
#include <TH2.h>
#include <TStyle.h>
#include <TFile.h>
#include <TCanvas.h>

void MyAna12::Loop(int mode)
{
	//   In a ROOT session, you can do:
	//      root> .L MyAna.C
	//      root> MyAna t
	//      root> t.GetEntry(12); // Fill t data members with entry number 12
	//      root> t.Show();       // Show values of entry 12
	//      root> t.Show(16);     // Read and show values of entry 16
	//      root> t.Loop();       // Loop on all entries
	//

	//     This is the loop skeleton where:
	//    jentry is the global entry number in the chain
	//    ientry is the entry number in the current Tree
	//  Note that the argument to GetEntry must be:
	//    jentry for TChain::GetEntry
	//    ientry for TTree::GetEntry and TBranch::GetEntry
	//
	//       To read only selected branches, Insert statements like:
	// METHOD1:
	//    fChain->SetBranchStatus("*",0);  // disable all branches
	//    fChain->SetBranchStatus("branchname",1);  // activate branchname
	// METHOD2: replace line
	//    fChain->GetEntry(jentry);       //read all branches
	//by  b_branchname->GetEntry(ientry); //read only this branch
	if (fChain == 0) return;

	bool applyetacut = false;
	bool applyidcut = false;
	bool applyisocut = false;
	TString rootfile = "histreco.root";

	if(mode == 1){
		rootfile = "histetacut.root"; 
		applyetacut = true;
	}
	if(mode == 2){
		rootfile = "histidcut.root";
		applyetacut = true;
		applyidcut = true;
	}
	if(mode == 3){
		rootfile = "histisocut.root";
		applyetacut = true;
		applyidcut = true;		
		applyisocut = true;
	}

	TFile rootFile(rootfile,"recreate");
	TH1D *h_photon_pt = new TH1D("photon_pt","photon_pt",100,0.,250.);
	TH1D *h_photon_eta = new TH1D("photon_eta","photon_eta",100,-3.,3.);
	TH1D *h_nphotons = new TH1D("Nphotons","Nphotons",50,0.,50.);
	TH1D *h_diphotonmass = new TH1D("diphotonmass","diphotonmass",100,0.,250.);
	TH1D *h_parentID = new TH1D("parentID","parentID",50,0.,50.);
	TH1D *h_photonID = new TH1D("photonID","photonID",50,0.,50.);
	TH1D *h_nPhotonID = new TH1D("nPhotonID","Number of photonID",11,-1.,10.);
	TH1D *h_nPhotonParentID = new TH1D("nPhotonParentID","Number of reconstructed photon photonID and parentID",11,-1.,10.);
	TH1D *h_Nmctruthphoton = new TH1D("Nmctruthphoton","Number of truth photon from Higgs",10,0.,10.);
	TH1D *h_L124Lleadingpt = new TH1D("L124Lleadingpt","leading photon pT passed L1_2eEM24L",100.,0.,200.);
	TH1D *h_L118Mleadingpt = new TH1D("L118Mleadingpt","leading photon pT passed L1_2eEM18M",100.,0.,200.);
	TH1D *h_L126Mleadingpt = new TH1D("L126Mleadingpt","leading photon pT passed L1_eEM26M",100.,0.,200.);
	TH1D *h_HLTg35leadingpt = new TH1D("HLTg35leadingpt","leading photon pT passed HLT_g35_g25M_L12eEM18M",100.,0.,200.);
	TH1D *h_HLTg20leadingpt = new TH1D("HLTg20leadingpt","leading photon pT passed HLT_g20T_L12eEM18M",100.,0.,200.);
	TH1D *h_HLTg22leadingpt = new TH1D("HLTg22leadingpt","leading photon pT passed HLT_g22T_L12eEM18M",100.,0.,200.);

	TH1D *h_L124Lsubleadingpt = new TH1D("L124Lsubleadingpt","subleading photon pT passed L1_2eEM24L",100.,0.,200.);
	TH1D *h_L118Msubleadingpt = new TH1D("L118Msubleadingpt","subleading photon pT passed L1_2eEM18M",100.,0.,200.);
	TH1D *h_L126Msubleadingpt = new TH1D("L126Msubleadingpt","subleading photon pT passed L1_eEM26M",100.,0.,200.);
	TH1D *h_HLTg35subleadingpt = new TH1D("HLTg35subleadingpt","subleading photon pT passed HLT_g35_g25M_L12eEM18M",100.,0.,200.);
	TH1D *h_HLTg20subleadingpt = new TH1D("HLTg20subleadingpt","subleading photon pT passed HLT_g20T_L12eEM18M",100.,0.,200.);
	TH1D *h_HLTg22subleadingpt = new TH1D("HLTg22subleadingpt","subleading photon pT passed HLT_g22T_L12eEM18M",100.,0.,200.);

	TH1D *h_leadingpt = new TH1D("leadingpt","reco leading pt",100,0.,200.);
	TH1D *h_subleadingpt = new TH1D("subleadingpt","reco subleading pt",100,0.,200.);

	//TH1D *h_tightleadingpt = new TH1D("tightleadingpt","tight leading pt",100,0.,200.);
	//TH1D *h_tightsubleadingpt = new TH1D("tightsubleadingpt","tight subleading pt",100,0.,200.);

	//TH1D *h_etacutleadingpt = new TH1D("etacutleadingpt","leading pt etacut",100,0.,200.);
	//TH1D *h_etacutsubleadingptEtacut = new TH1D("etacutsubleadingpt","subleading pt etacut",100,0.,200.);

	Long64_t nentries = fChain->GetEntriesFast();

	Long64_t nbytes = 0, nb = 0;

	int nPhotons;

	float leadingPt;
	float subleadingPt;
	int leadingindex;
	int subleadingindex;

	TLorentzVector leadingphotonLV, subleadingphotonLV;

	int nPhotonID;
	int nPhotonParentID;

	for (Long64_t jentry=0; jentry<nentries;jentry++) {

		Long64_t ientry = LoadTree(jentry);
		if (ientry < 0) break;
		nb = fChain->GetEntry(jentry);   nbytes += nb;

		nPhotons = photonPt->size();
		h_nphotons->Fill(nPhotons);
		if(nPhotons < 2) continue;

		leadingPt = 0;
		subleadingPt = 0;
		leadingindex = -1;
		subleadingindex = -1;
		nPhotonID = 0;
		nPhotonParentID = 0;

		for(int iPhoton=0;iPhoton<nPhotons;++iPhoton){
			h_photon_pt->Fill(photonPt->at(iPhoton));
			h_photon_eta->Fill(photonEta->at(iPhoton));

			h_photonID->Fill(photonPdgId->at(iPhoton));
			h_parentID->Fill(parentPdgId->at(iPhoton));			

			if((photonPdgId->at(iPhoton) != 22)) continue; // photon pdgId ==22 Higgs PdgId == 25
			nPhotonID++;

			if((parentPdgId->at(iPhoton) != 25)) continue;
			nPhotonParentID++;

			if(photonPt->at(iPhoton) > leadingPt){
				subleadingPt = leadingPt;
				leadingPt = photonPt->at(iPhoton);
				subleadingindex = leadingindex;
				leadingindex = iPhoton;
			}else if(photonPt->at(iPhoton) > subleadingPt){
				subleadingPt = photonPt->at(iPhoton);
				subleadingindex = iPhoton;
			}
		}

		h_nPhotonID->Fill(nPhotonID);
		h_nPhotonParentID->Fill(nPhotonParentID);

		if((leadingindex < 0) || (subleadingindex < 0)) continue;
		if(applyetacut && (!etacut->at(leadingindex) || !etacut->at(subleadingindex))) continue;
		if(applyidcut && (!isTight->at(leadingindex) || !isTight->at(subleadingindex))) continue;
		if(applyisocut && (!isoSelection->at(leadingindex) || !isoSelection->at(subleadingindex))) continue;

		h_leadingpt->Fill(photonPt->at(leadingindex));
		h_subleadingpt->Fill(photonPt->at(subleadingindex));

		if(L12eEM24L){
			h_L124Lleadingpt->Fill(photonPt->at(leadingindex));	
			h_L124Lsubleadingpt->Fill(photonPt->at(subleadingindex));
		}

		if(L12eEM18M){
			h_L118Mleadingpt->Fill(photonPt->at(leadingindex));
			h_L118Msubleadingpt->Fill(photonPt->at(subleadingindex));
		}

		if(L1eEM26M){
			h_L126Mleadingpt->Fill(photonPt->at(leadingindex));
			h_L126Msubleadingpt->Fill(photonPt->at(subleadingindex));
		}

		if(HLTg35g25M_L124L){
			h_HLTg35leadingpt->Fill(photonPt->at(leadingindex));
			h_HLTg35subleadingpt->Fill(photonPt->at(subleadingindex));
		}

		if(HLT2g20T_L118M){
			h_HLTg20leadingpt->Fill(photonPt->at(leadingindex));
			h_HLTg20subleadingpt->Fill(photonPt->at(subleadingindex));
		}

		if(HLT2g22T_L118M){
			h_HLTg22leadingpt->Fill(photonPt->at(leadingindex));
			h_HLTg22subleadingpt->Fill(photonPt->at(subleadingindex));
		}


		leadingphotonLV.SetPtEtaPhiE(photonPt->at(leadingindex),photonEta->at(leadingindex),photonPhi->at(leadingindex),photonE->at(leadingindex));
		subleadingphotonLV.SetPtEtaPhiE(photonPt->at(subleadingindex),photonEta->at(subleadingindex),photonPhi->at(subleadingindex),photonE->at(subleadingindex));
		float diphotonmass = (leadingphotonLV+subleadingphotonLV).M();
		h_diphotonmass->Fill(diphotonmass);
		//h_subleadingpt->Fill(photonPt->at(subleadingindex));
		//if((L12eEM24L) && (photonPt->at(leadingindex) > 24))
		//	h_L1leadingpt->Fill(photonPt->at(leadingindex));
		// if (Cut(ientry) < 0) continue;
	}
	rootFile.Write();
	rootFile.Close();
}
